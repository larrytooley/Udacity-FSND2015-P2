-- Table definitions for the tournament project.

-- Ensure that a fresh database is generated.
DROP DATABASE IF EXISTS tournament;


-- Create new database.
CREATE DATABASE tournament;


-- Connect to the database.
\c tournament;


-- Create player table.
CREATE TABLE players ( id SERIAL PRIMARY KEY,
                       name TEXT);


-- Create match table.
CREATE TABLE matches ( id SERIAL PRIMARY KEY,
                       winner INTEGER REFERENCES players (id),
                       loser INTEGER REFERENCES players (id));


-- View to count the number of wins for each player.
CREATE VIEW wins AS SELECT players.id as id, players.name as name, count(matches.winner) as wins
                    FROM players
                    LEFT JOIN matches 
                    ON players.id = matches.winner 
                    GROUP BY players.id 
                    ORDER BY Wins DESC, 
                             players.id;


-- -- View to determine the player standings.
CREATE VIEW standings AS SELECT players.id, players.name, count(w.winner) as wins,
                                count(w.winner) + count(l.loser) as matches
                         FROM players
                         LEFT JOIN matches w ON players.id = w.winner
                         LEFT JOIN matches l ON players.id = l.loser
                         GROUP BY players.id
                         ORDER by wins DESC;