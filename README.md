# Tournament

Code to implement a Swiss System tournament match maker as part of [Udacity's](https://www.udacity.com) Full Stack Web Developer Nanodegree.

[Project Guide](https://docs.google.com/document/d/16IgOm4XprTaKxAa8w02y028oBECOoB1EI1ReddADEeY/pub?embedded=true)

## Table of Contents
* [Files](#files)
* [How To Run](#how-to-run)
* [Database Set Up and Testing](#database-set-up-and-testing)
* [Python Code Testing](#python-code-testing)

## Files

* **tournament.sql** - This file contains the database schema.

* **tournament.py** - This file is the main program used to connect to the database and add players and matches.

* **tournament_test.py** - This file contains the tests to determine if tournament.py is working as expected.

* **players.sql** - This file contains players for testing.  It should not be loaded when using the program normally.

* **matches.sql** - This file contains players for testing.  It should not be loaded when using the program normally.

[Back to Top](#tournament)


## How To Run

The code for this project can be run either inside the Udacity Vagrant VM.  Additional step will be required to run in a stand alone environment.

### Udacity Vagrant VM

Follow the guide found [here](https://www.udacity.com/wiki/ud197/install-vagrant) to install and run the VM.

### Installation
* Download the ZIP by clicking on the link on the right side of this page.
* Exctract the archive and copy the files to a directory inside the vagrant directory.

**OR**

* Clone the repository inside the vagrant directory.

 ```
 git clone https://github.com/larrytooley/Udacity-FSND2015-P2.git
 ```

[Back to Top](#tournament)

## Database Set Up and Testing

To create and test the database run the following commands in the installation directory:

* Start the Postgres commandline tool, **psql**.

```
vagrant tournament $ psql
psql (9.3.9)
Type "help" for help.

vagrant=>
```

* Import the **tournament.sql** file containing the database schema.

```pgsql
vagrant=> \i tournament.sql
DROP DATABASE
CREATE DATABASE
You are now connected to database "tournament" as user "vagrant".
CREATE TABLE
CREATE TABLE
CREATE VIEW
CREATE VIEW
tournament=>
```

* Import the **players.sql** test data.

```pgsql
tournament=> \i player.sql
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
tournament=>
```

* You should now see the following when you select all the data from the players table.

```pgsql
SELECT * FROM players;
```
* Should output:

```
id | name
----+------
  1 | p1
  2 | p2
  3 | p3
  4 | p4
  5 | p5
  6 | p6
  7 | p7
  8 | p8
  9 | p9
 10 | p10
 11 | p11
 12 | p12
 13 | p13
 14 | p14
 15 | p15
 16 | p16
(16 rows)
```
* Import the **matches.sql** test data.

```pgsql
tournament=> \i matches.sql
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
INSERT 0 1
tournament=>
```
* You should now see the following when you select all the data from the **matches** table.

```pgsql
SELECT * FROM matches;
```
* Should output:

```
id | winner | loser
----+--------+-------
  1 |     16 |    15
  2 |      6 |     8
  3 |      9 |     4
  4 |     14 |    12
  5 |      7 |     5
  6 |     10 |    11
  7 |      2 |     3
  8 |     13 |     1
  9 |     15 |     6
 10 |      9 |    14
 11 |      7 |    10
 12 |      2 |    13
 13 |     16 |     8
 14 |      4 |    12
 15 |      5 |    11
 16 |      3 |     1
(16 rows)
```
* Check that the **wins** view is working.

```pgsql
tournament=> SELECT * FROM wins;
```
* Should output:

```
id | name | wins
----+------+------
  2 | p2   |    2
  7 | p7   |    2
  9 | p9   |    2
 16 | p16  |    2
  3 | p3   |    1
  4 | p4   |    1
  5 | p5   |    1
  6 | p6   |    1
 10 | p10  |    1
 13 | p13  |    1
 14 | p14  |    1
 15 | p15  |    1
  1 | p1   |    0
  8 | p8   |    0
 11 | p11  |    0
 12 | p12  |    0
(16 rows)
```
* Check that the **standings** view is working.

```pgsql
tournament=> SELECT * FROM standings;
```
* Should output:

```
id | name | wins | matches
----+------+------+---------
 7 | p7   |    2 |       2
16 | p16  |    2 |       2
 2 | p2   |    2 |       2
 9 | p9   |    2 |       2
13 | p13  |    1 |       2
15 | p15  |    1 |       2
 6 | p6   |    1 |       2
 5 | p5   |    1 |       2
 4 | p4   |    1 |       2
 3 | p3   |    1 |       2
14 | p14  |    1 |       2
10 | p10  |    1 |       2
12 | p12  |    0 |       2
 8 | p8   |    0 |       2
 1 | p1   |    0 |       2
11 | p11  |    0 |       2
(16 rows)
```

[Back to Top](#tournament)

## Python Code Testing

* If you are still in **psql**, exit out and run **tournament_test.py** to test if the functions implemented in **tournament.py** are working according to the specification.

```
tournament=> \q
vagrant tournament $ python tournament_test.py
1. Old matches can be deleted.
2. Player records can be deleted.
3. After deleting, countPlayers() returns zero.
4. After registering a player, countPlayers() returns 1.
5. Players can be registered and deleted.
6. Newly registered players appear in the standings with no matches.
7. After a match, players have updated standings.
8. After one match, players with one win are paired.
Success!  All tests pass!
vagrant tournament $
```
[Back to Top](#tournament)
