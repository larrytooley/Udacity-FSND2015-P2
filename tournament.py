#!/usr/bin/env python
#
# tournament.py -- implementation of a Swiss-system tournament
#

import psycopg2
import bleach
from contextlib import contextmanager


def connect():
    """Connect to the PostgreSQL database.  Returns a database connection."""
    return psycopg2.connect("dbname=tournament")


@contextmanager
def get_cursor():
    """Manage database connection."""
    db = connect()
    c = db.cursor()
    try:
        yield c
    except:
        raise
    else:
        db.commit()
    finally:
        db.close()


def deleteMatches():
    """Remove all the match records from the database."""
    with get_cursor() as cur:
        cur.execute("DELETE FROM matches")


def deletePlayers():
    """Remove all the player records from the database."""
    with get_cursor() as cur:
        cur.execute("DELETE FROM players")


def countPlayers():
    """Returns the number of players currently registered."""
    with get_cursor() as cur:
        cur.execute("SELECT count(id) FROM players")
        player_count = cur.fetchall()
        return player_count[0][0]


def registerPlayer(name):
    """Adds a player to the tournament database.

    The database assigns a unique serial id number for the player.  (This
    should be handled by your SQL database schema, not in your Python code.)

    Args:
      name: the player's full name (need not be unique).
    """
    name = bleach.clean(name, strip=True)
    with get_cursor() as cur:
        cur.execute("INSERT INTO players (name) VALUES (%s);", (name,))


def playerStandings():
    """Returns a list of the players and their win records, sorted by wins.

    The first entry in the list should be the player in first place,
    or a player tied for first place if there is currently a tie.

    Returns:
      A list of tuples, each of which contains (id, name, wins, matches):
        id: the player's unique id (assigned by the database)
        name: the player's full name (as registered)
        wins: the number of matches the player has won
        matches: the number of matches the player has played
    """
    with get_cursor() as cur:
        cur.execute("SELECT * FROM standings;")
        standings = cur.fetchall()
        return standings


def reportMatch(winner, loser):
    """Records the outcome of a single match between two players.

    Args:
      winner:  the id number of the player who won
      loser:  the id number of the player who lost
    """
    with get_cursor() as cur:
        cur.execute("INSERT INTO matches (winner, loser) VALUES ((%s), (%s));",
                   (winner, loser,))


def swissPairings():
    """Returns a list of pairs of players for the next round of a match.

    Assuming that there are an even number of players registered, each player
    appears exactly once in the pairings.  Each player is paired with another
    player with an equal or nearly-equal win record, that is, a player adjacent
    to him or her in the standings.

    Returns:
      A list of tuples, each of which contains (id1, name1, id2, name2)
        id1: the first player's unique id
        name1: the first player's name
        id2: the second player's unique id
        name2: the second player's name
    """
    standings = playerStandings()
    pairings = []

    for i in range(len(standings)/2):
        if i == 0 or i % 2 == 0:    # Appends the zero and even players
            pairings.append((standings[i][0], standings[i][1],
                             standings[i+1][0], standings[i+1][1]))
        else:                       # Offset to append odd players
            pairings.append((standings[i+1][0], standings[i+1][1],
                             standings[i+2][0], standings[i+2][1]))
    return pairings
